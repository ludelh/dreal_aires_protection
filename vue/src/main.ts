import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import Oruga from '@oruga-ui/oruga-next'
import { bulmaConfig } from '@oruga-ui/theme-bulma'
import './assets/scss/main.scss'
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'


library.add(fas)
dom.watch()

const app = createApp(App)

app.use(
  createPinia()  
)
app.use(
  Oruga,
  {
    ...bulmaConfig,
    iconPack: 'fas'
  }  
)
app.mount('#app')
