import { defineStore } from 'pinia'
import { parse } from 'papaparse'
import type { surfaceInterface }  from '@/interfaces/surfaces';

export type SurfaceState = {
  data: surfaceInterface[];
};

export const useSurfacesStore = defineStore({
  id: 'surfaces',
  state: () => ({
    data:[]
  }) as SurfaceState,
  actions: {
    async getData() {
      const response = await fetch(`${import.meta.env.VITE_PLUGIN_PATH}/data/surfaces.csv`)
      const text = await response.text()
      const parseData = parse(text,{header:true}).data.map((d:any) => {
        return {
          annee: d.annee,
          dep: d.dep,
          type_site: d.type_site,
          surface_ha: parseFloat(d.surf_cumul),
          percent_dep: d.percent_dep
        }
      })
      this.data = parseData
    },
  },
})