import { defineStore } from 'pinia'

export const useParametersStore = defineStore({
  id: 'parameters',
  state: () => ({
    showArea: {
      pnr: true,
      prot_default: true,
      prot_forte: true,
      n2000:true,
    },
    showTable: false,
    timelapse: {
      current: new Date().getTime(),
      min: new Date('1955-01-01').getTime(),
      max: new Date().getTime()
    },
    sumSurface: 0,
    selectedDep: null,
    depList:[
      {dep:'01',l_dep:'Ain'},
      {dep:'03',l_dep:'Allier'},
      {dep:'07',l_dep:'Ardèche'},
      {dep:'15',l_dep:'Cantal'},
      {dep:'26',l_dep:'Drôme'},
      {dep:'38',l_dep:'Isère'},
      {dep:'42',l_dep:'Loire'},
      {dep:'43',l_dep:'Haute-Loire'},
      {dep:'63',l_dep:'Puy-de-Dôme'},
      {dep:'69',l_dep:'Rhône'},
      {dep:'73',l_dep:'Savoie'},
      {dep:'74',l_dep:'Haute-Savoie'},
    ],
    typeList:[
      {id:'CPN',name:'Coeur de parc national (prot. Forte)',order:1},
      {id:'AAPN',name:'Aire d’adhésion de parc national',order:2},
      {id:'RNN',name:'Réserve naturelle nationale (prot. Forte)',order:3},
      {id:'RNR',name:'Réserve naturelle régionale (prot. Forte)',order:4},
      {id:'RBI',name:'Réserve biologique forestière intégrale (prot. Forte)',order:5},
      {id:'RBD',name:'Réserve biologique forestière dirigée (prot. Forte)',order:6},
      {id:'APB-APHN',name:'Arrêté de protection de biotope ou d’habitats naturels (prot. Forte)',order:7},
      {id:'RNCFS',name:'Réserve nationale de chasse et de faune sauvage',order:8},
      {id:'SCL',name:'Site du conservatoire du littoral',order:9},
      {id:'CEN',name:'Site géré par un conservatoire des espaces naturels',order:10},
      {id:'PNR',name:'Parc naturel régional',order:11},
      {id:'N2000',name:'Natura 2000',order:12},
      {id:'RAMSAR',name:'Site RAMSAR',order:13},
      {id:'UNESCO',name:'Site UNESCO',order:14},
      {id:'MAB',name:'Réserve de biosphère',order:15},      
      {id:'_AP-tot',name:'Total aires protégées (sans double compte, APF incluses)',order:16},
      {id:'_APF-tot',name:'Total aires sous protection forte (sans double compte)',order:17},
    ]    
  })
})