export interface surfaceInterface{
  annee: string,
  dep: string,
  type_site: string,
  surface_ha: number,
  percent_dep: number,
}
