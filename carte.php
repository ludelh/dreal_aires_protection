<?php
/**
 *  Plugin Name: Carte d'évolution des surfaces protégées
 */
add_shortcode('carte_surfaces_protegees', 'carte_shortcode');

function carte_shortcode(){
  $content = '<div id="app"></div>';
  return $content;
};

function enqueue_vue_scripts() {
  global $post;
  if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'carte_surfaces_protegees') ) {
      wp_enqueue_script('carte_surfaces_protegees', plugin_dir_url( __FILE__ ) . '/dist/assets/index.js', [], '1.0', true);
      wp_enqueue_style( 'carte_surfaces_protegees', plugin_dir_url( __FILE__ ) . '/dist/assets/index.css', [], '1.0.0', 'all' );
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_vue_scripts');

?>